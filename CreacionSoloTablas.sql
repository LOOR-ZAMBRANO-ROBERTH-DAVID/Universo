create table cliente(
	cliente_id varchar(10) primary key not null,
	cliente_nombre varchar(20) not null,
	cliente_apellido varchar(20) not null,
	cliente_telefono varchar(10),
	cliente_fecha_nacimiento date,
	cliente_estado_civil varchar(10),
	cliente_genero varchar(10),
	cliente_nacionalidad varchar(20),
	cliente_direccion varchar(50) not null,
	cliente_numero_hijos numeric
);

create table hijo(
	hijo_id varchar(10) primary key not null,
	hijo_nombre varchar(20) not null,
	hijo_apellido varchar(20) not null,
	hijo_genero varchar (10),
	hijo_fecha_de_nacimiento date,
	hijo_nacionalidad varchar(20),
	hijo_disgusto varchar (50),
	
	hijo_cliente_id varchar(10) not null,
	constraint cliente_fk foreign key (hijo_cliente_id) references cliente (cliente_id)	
);

create table nanny(
	nanny_id varchar(10) primary key not null,
	nanny_nombre varchar(20) not null,
	nanny_apellido varchar(20) not null,
	nanny_estado_civil varchar(10),
	nanny_nacionalidad varchar (11),
	nanny_ocupacion varchar (50),
	nanny_numero_hijos numeric,
	nanny_tiempo_laborando varchar(20),
	nanny_carrera_universitaria varchar(50)
	
);

create table servicio(
	servicio_id varchar(4),
	sevricio_pago_realizado float,
	servicio_cant_hijos numeric,
	servicio_horas numeric,
	servicio_fecha date,
	servicio_inconvenientes varchar(50),
	servicio_calificacion numeric,
	
	servicio_cliente_id varchar(10),
	servicio_nanny_id varchar(10),
	constraint cliente_fk foreign key (servicio_cliente_id) references cliente(cliente_id),
	constraint nanny_fk foreign key (servicio_nanny_id) references nanny(nanny_id)
	
);

--Se crean las alergias
create table alergia(
	alergia_id varchar (5) primary key not null,
	alergia_nombre varchar (20) not null
);



create table alergia_hijo(
	alergia_hijo_id varchar(5),
	
	alergia_hijo_alergia_id varchar (5) not null,
	alergia_hijo_hijo_id varchar (20),
	constraint alergia_hijo_fk foreign key (alergia_hijo_hijo_id) references hijo(hijo_id),
	constraint alergia_alergia_fk foreign key (alergia_hijo_alergia_id) references alergia(alergia_id)
);

create table sintoma_alergia(
	sintoma_alergia_id varchar (3) primary key not null,
	sintoma_alergia_alergia_id varchar (5),
	sintoma_alergia_descripcion varchar (50),
	
	constraint alergia_fk foreign key (sintoma_alergia_alergia_id) references alergia(alergia_id)
);


create table tratamiento_alergia(
	tratamiento_alergia_id varchar(4) primary key not null,
	tratamiento_alergia_alergia_id varchar(5),
	tratamiento_alergia_descripcion varchar(200),
	
	constraint alergia_fk foreign key (tratamiento_alergia_alergia_id) references alergia(alergia_id)
);


